# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

<Jens Scheel> -- <project manager>
<Carsten Coull> -- <developer>
<Johannes Braun> -- <developer>


# TECHNOLOGY COLOPHON

CSS3, HTML5, PHP, MySQL/MariaDB, Content-o-mat
