<?php
namespace Contentomat\MLog;

use \Contentomat\PsrAutoloader;
use \Contentomat\Controller;
use \Contentomat\DBCex;
use \Contentomat\Contentomat;
use \Contentomat\Parser;
use \Contentomat\Paging;

use \Contentomat\MLog\Posts;


/**
 * Class mlogView .. Local Class extends Controller 
 */
class MLogController extends Controller {

	protected $postId;
	protected $categoryId;
	protected $categoryData;
	protected $searchText;
	protected $searchIn;
	protected $mlog;
	protected $posts;
	protected $comments;
	protected $mlogTemplatePath;
	protected $commentsTemplatePath;
	protected $currentPage;
	protected $pagingLinks = 10;
	protected $authors;
	protected $entriesPerPage = 5;
	protected $postRelations;
	
	protected $overviewPageId = 10;
	protected $detailsPageId = 10;
	
	protected $cmt;

	public function init() {

		$this->cmt->addAutoloadNamespace('Contentomat\MLog\\', INCLUDEPATHTOADMIN . 'classes/app_mlog');
		
		// templates path
		// TODO: move to MLog settings/ application settings! Initiate new ApplicationHandler instance and get settings for 'MLog'
		// $this->mlogTemplatePath = $this->cmtSettings['templatesPath']
		$this->mlogTemplatePath = $this->templatesPath . 'mlog/';
		$this->commentsTemplatePath = $this->templatesPath . 'comments/';

		// properties
		$this->postId = intval($_REQUEST['aid']); // post id
		$this->categoryId = intval($_REQUEST['cat']); // article category id

		if ($this->postId) {
			$this->action = 'showPostDetails';
		}
		
		$this->searchText = $_REQUEST['search']; // such begriff
		
		$this->currentPage = intval($this->getvars['cp']);
		if (!$this->currentPage) {
			$this->currentPage = 1;
		}

		$this->posts = new Posts();
		$this->authors = new Authors();
		$this->categoryData = $this->posts->getCategoryData($this->categoryId);
	}




	/**
	 * protected function actionShowPostsList()
	 * Shows a list of posts in the seleced category
	 * 
	 * @param void
	 * @return void
	 */
	public function actionDefault() {

		if ($this->categoryData){
			define('POSTSCATEGORY',': '.$this->categoryData['category_title_' . PAGELANG] );
		}
		
		$condWhere = array();
		$searchFields = 'post_text,post_tags,post_title';
		$rowTemplate = 'mlog_article_row.tpl';
		$frameTemplate = 'mlog_article_frame.tpl';

		// does a search term exist?
		if ($this->searchText != '') {
			$searchText = $this->searchText;
			$searchMode = True;
		}


		if ($this->searchIn != '') {

			switch ($this->searchIn) {
				case 'tags':
					$searchFields = 'post_tags';
					break;
				case 'authors':
					$searchFields = 'post_author';
					$condWhere[] = "post_author='" . $this->searchTextRequest . "'";
					break;
			}
		}

		$contentData = $this->cmt->getVar('cmtContentData');
		mb_internal_encoding('UTF-8');
		
		$entriesPerPage = intval($contentData['head1']);
		$rt = preg_replace('/\s+/u', '', strip_tags($contentData['head2']));
		$ft = preg_replace('/\s+/u', '', strip_tags($contentData['head3']));		

		if ($rt) {
			$rowTemplate = $rt;
		}
		
		
		if ($ft) {
			$frameTemplate = $ft;
		}
		
		if ($entriesPerPage) {
			$this->entriesPerPage = $entriesPerPage;
		}

		$posts = $this->posts->search(array(
			'orderDir' => 'DESC',
			'orderBy' => 'post_online_date',
			'condWhere' => $condWhere,
			"searchText" => $searchText,
			"searchMode" => $searchMode,
			"searchFields" => $searchFields,
			"searchType" => 'like',
			"teaserFieldPriority" => 'post_text',
			"postsPerPage" => $this->entriesPerPage,
			'pagingShowLinks' => $this->pagingLinks,
			'currentPage' => $this->currentPage,
			'categoryLimit' => $this->categoryId
		));

		if (!empty($posts)) {

			foreach ($posts as $post) {

				// Post belongs Categories
				$categoryBelongsContent = array();
				
				$this->parser->setParserVar('search', $this->searchTextRequest);
				$this->parser->setParserVar('searchIn', $this->searchIn);
				$this->parser->setParserVar('categoryId', $this->categoryId);
				$this->parser->setMultipleParserVars($post);
				$this->parser->setMultipleParserVars($post['hasMedia']);
				$this->parser->setMultipleParserVars($this->mlog->paging);
				$this->parser->setParserVar('categoryBelongsContent', join(" / ", $categoryBelongsContent));
				$this->parser->setParserVar('lastCommentsList', $lastCommentsList);
				$contentList .= $this->parser->parseTemplate($this->mlogTemplatePath . $rowTemplate);
			}
		}

		$this->parser->setParserVar('pagingContent', $this->createPaging());
		$this->parser->setParserVar('contentList', $contentList);
		$this->parser->setParserVar('search', $this->searchText);
		$this->content = $this->parser->parseTemplate($this->mlogTemplatePath . $frameTemplate);
	}

	protected function createPaging() {
		$pagingContent = '';
		$paging = new Paging();
		$pagingData = $paging->makePaging(array(
			'pagingLinks' => $this->pagingLinks,
			'entriesPerPage' => $this->entriesPerPage,
			'totalEntries' => $this->posts->getTotalPosts(),
			'currentPage' => $this->currentPage
				));
		$pagingContent = '';
		$pagingTemplate = file_get_contents($this->mlogTemplatePath . 'mlog_paging.tpl');
		$pagingTemplate = explode("{SPLITTEMPLATEHERE}", $pagingTemplate);

		if ($this->categoryData && !empty($this->categoryData)) {
			$this->parser->setParserVar('pageId', $this->pageId);
			$this->parser->setParserVar('categoryId', $this->categoryData['id']);
			$this->parser->setParserVar('categoryName', ucfirst($this->categoryData['category_name']));
		} else {
			$this->parser->setParserVar('pageId', PAGEID);
			$this->parser->setParserVar('categoryId', '0');
			$this->parser->setParserVar('categoryName', '');
		}

		if ($pagingData['prevPage']) {
			$this->parser->setParserVar('prevPage', $pagingData['prevPage']);
			$pagingContent .= $this->parser->parse($pagingTemplate[0]);
		}

		foreach ($pagingData['pages'] as $page) {
			$this->parser->setParserVar('listItem', $page);

			$this->parser->setParserVar('selected', $page == $this->currentPage);
			$pagingContent .= $this->parser->parse($pagingTemplate[1]);
		}

		if ($pagingData['nextPage']) {
			$this->parser->setParserVar('nextPage', $pagingData['nextPage']);
			$pagingContent .= $this->parser->parse($pagingTemplate[2]);
		}
		
		return $pagingContent;
	}

	protected function actionShowPostDetails() {
		$actionContent = '';

		// show article details
		$post = $this->posts->getPost(array('postID' => $this->postId));
		
		define('POSTTITLE', htmlspecialchars($post['post_title']));

		//Get all the relevant date parameters
		$date = date_create(date($post['post_online_date']));
		$post['post-date__year'] =$date->format('Y');
		$post['post-date__month-description'] =$date->format('M');
		$post['post-date__month'] =$date->format('m');
		$post['post-date__day'] =$date->format('d');
		$post['post-date__day-description'] =$date->format('D');
		if($this->categoryData){
			define('POSTSCATEGORY',$this->categoryData['category_title_de'].' - ');
		}
		
		// update Article's total views status
		$this->posts->updatePostStatus($this->postId);

		// Article Details Ad
//		$detailsInnerAd = $this->parser->parseTemplate($this->mlogTemplatePath . 'mlog_article_details_ad.tpl');



		$post['post_text'] = preg_replace('/^(.*?)\<\/p>/', '$1</p>' . $detailsInnerAd, $post['post_text']);

		// JH: Kategoriedaten auslesen!
		$this->categoryData = $this->posts->getCategoryData($this->categoryId);

		$this->parser->setMultipleParserVars($post['hasMedia']);

		$mediaContent = array();


		if ($post['postMedia']) {
			foreach ($post['postMedia'] as $mediaType => $mediaItems) {

				foreach ($mediaItems as $media) {
					$this->parser->setParserVar('mediaType', strtolower($mediaType));
					$this->parser->setMultipleParserVars($media);
					$mediaContent[$mediaType][] = $this->parser->parseTemplate($this->mlogTemplatePath . 'mlog_article_media.tpl');
				}
			}
		}

		if ($mediaContent) {
			foreach ($mediaContent as $mediaType => $mediaContent) {
				$this->parser->setParserVar('contentList' . ucfirst($mediaType), join("\n", $mediaContent));
			}
		}


		switch ($post['post_options']) {
			case '1':
				$this->parser->setParserVar('mediaBarTop', 1);
				break;
			case '2':
				$this->parser->setParserVar('mediaBarBottom', 1);
				break;
			case '3':
				$this->parser->setParserVar('mediaBarTop', 1);
				$this->parser->setParserVar('mediaBarBottom', 1);
				break;
			default:
				break;
		}

		// Post belongs Categories
		$categoryBelongsContent = array();

		
		foreach ($post['post_categories'] as $postCategory) {
			$this->parser->setMultipleParserVars($postCategory);
			$this->parser->setParserVar('categoryName', ucfirst($postCategory['category_name']));
			$categoryBelongsContent[] = $this->parser->parseTemplate($this->mlogTemplatePath . 'mlog_category_link.tpl');
		}

		$this->parser->setParserVar('categoryBelongsContent', join(" / ", $categoryBelongsContent));

		if ($this->categoryData && !empty($this->categoryData)) {
			$this->parser->setParserVar('categoryId', $this->categoryData['id']);
			$this->parser->setParserVar('categoryInternalName', $this->categoryData['category_name']);
			$this->parser->setParserVar('categoryName', $this->categoryData['category_title_de']);
		} else {
			$this->parser->setParserVar('categoryId', 0);
			$this->parser->setParserVar('categoryInternalName', '');
			$this->parser->setParserVar('categoryName', '');
		}

		if ($post['post_author']) {
			$authorData = $this->authors->getAuthor(array('authorId' => $post['post_author']));
			$this->parser->setParserVar('author_name', $authorData['author_name']);
			$this->parser->setParserVar('author_prename', $authorData['author_prename']);
			$this->parser->setParserVar('authorId', $authorData['id']);
			$this->parser->setParserVar('authorLinkName', $this->cmt->makeNameWebsave($authorData['author_prename'] . ' ' . $authorData['author_name']));
		}
		
		
		// Post Relations
		$relationListContent = '';
		if (is_array($this->postRelations) && !empty($this->postRelations)) {
			foreach ($this->postRelations as $relPost) {
				$this->parser->setMultipleParserVars($relPost);
				$this->parser->setParserVar('linkTitle',  $this->cmt->makeNameWebsave($relPost['post_title']));
				$relationListContent .= $this->parser->parseTemplate($this->mlogTemplatePath . 'mlog_relations_row.tpl');
			}

			if($relationListContent){
				$this->parser->setParserVar('relationListContent',$relationListContent);
				$furtherInformation = $this->parser->parseTemplate($this->mlogTemplatePath.'mlog_relations_frame.tpl');
			}
			
		}

		$this->parser->setMultipleParserVars($post);

		$this->parser->setParserVar('furtherInformation', $furtherInformation);
		$this->parser->setParserVar('currentPage', $this->currentPage);
		$this->parser->setParserVar('search', $this->searchTextRequest);
		$this->parser->setParserVar('searchIn', $this->searchIn);

		// Parse Details Template
		$actionContent .= $this->parser->parseTemplate($this->mlogTemplatePath . 'mlog_article_details.tpl');



		// show comments
		if ($this->comments->commentable && $post['post_comment_status'] == '1') {

			
			$actionContent .= $this->comments->showCommentsList();

			if ($this->errorMessage[$post['id']]) {
				$actionContent .= $this->errorMessage[$post['id']];
			}
			
			if ($this->errorMessage['commentMessage']) {
				$actionContent .= $this->errorMessage['commentMessage'];
			}
			

			if ($this->showNewCommentForm) {
				$this->parser->setMultipleParserVars($this->commentVars);
				$this->parser->setParserVar('articleId', $post['id']);
				$actionContent .= $this->parser->parseTemplate($this->commentsTemplatePath . 'comments_form.tpl');
			} else {
				$this->parser->setParserVar('articleId', $commentPostId);
				// $actionContent .= $this->parser->parseTemplate($this->commentsTemplatePath . 'comments_confirm.tpl');
			}
		}

		$this->content = $actionContent;
	}
}


$controller = new MLogController();
$content .= $controller->work();
?>
