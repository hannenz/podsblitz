<?php
namespace Contentomat;


/**
 * This class can be used to extend Contentomat's model with project specific
 * functions
 *
 * @class AppModel
 * @author Carsten Coull <c.coull@agentur-halma.de>
 * @package contentomat
 * @version 2021-09-17
 */
class AppModel extends Model {
}