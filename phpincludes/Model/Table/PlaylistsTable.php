<?php
/**
 * phpincludes/Model/Table/PlaylistsTable.php
 *
 * @author Johannes Braun <hannenz@posteo.de>>
 * @package podsblitz
 * @version 2022-10-08
 */
namespace Podsblitz\Table;

use Podsblitz\Entity\Playlist;
use Podsblitz\Table\AppTable;

use Cake\ORM\Table;

class PlaylistsTable extends AppTable {

	public function initialize(array $config) : void {
		parent::initialize($config);

		$this->setupCakeModel();
		$this->setEntityClass(Playlist::class);

		$this->belongsToMany('Episodes', [
			'className' => 'Podsblitz\Table\EpisodesTable',
			'joinTable' => 'playlists_episodes',
			'sort' => ['pos' => 'asc'],
			'propertyName' => 'episodes',
			'saveStrategy' => 'append'
			// 'foreignKey' => 'table_parent_id',
			// 'bindingKey' => 'id',
			// 'targetForeignKey' => 'table_child_id',
			// 'conditions' => [
			// 	'CmtTablesTable.table_parent_table' => $this->getTableId()
			// ],
		]);

		$this->belongsTo('Users')
			->setForeignKey('playlist_user_id');

		$this->addBehavior('Timestamp', [
			'events' => [
				'Model.beforeSave' => [
					'playlist_created' => 'new'
				]
			]
		]);
	}
}

