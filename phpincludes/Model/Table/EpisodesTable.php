<?php
/**
 * phpincludes/Model/Table/EpisodesTable.php
 *
 * @author Johannes Braun <hannenz@posteo.de>>
 * @package podsblitz
 * @version 2022-10-08
 */
namespace Podsblitz\Table;

use Podsblitz\Entity\Episode;
use Podsblitz\Table\AppTable;

class EpisodesTable extends AppTable {

	public function initialize(array $config) : void {
		parent::initialize($config);

		$this->setEntityClass(Episode::class);

		$this->belongsTo('Subscriptions')
			->setForeignKey('episode_subscription_id');

		$this->addBehavior('Timestamp');

		$this->setupCakeModel();
	}
}
