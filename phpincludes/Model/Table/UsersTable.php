<?php
/**
 * phpincludes/Model/Table/UsersTable.php
 *
 * @author Johannes Braun <hannenz@posteo.de>>
 * @package podsblitz
 * @version 2022-10-08
 */
namespace Podsblitz\Table;

use Podsblitz\Entity\User;
use Podsblitz\Table\AppTable;

class UsersTable extends AppTable {

	public function initialize(array $config) : void {
		parent::initialize($config);

		$this->setEntityClass(User::class);
		$this->setTable('cmt_users');

		$this->hasMany('Subscriptions')
			->setForeignKey('subscription_user_id');

		$this->hasMany('Playlists')
		   ->setForeignKey('playlist_user_id');	

		$this->setupCakeModel();
	}
}

