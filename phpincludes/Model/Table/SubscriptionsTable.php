<?php
namespace Podsblitz\Table;

use Podsblitz\Entity\Subscription;
use Podsblitz\Entity\Episode;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Utility\Xml;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use Cake\Log\Log;
use Contentomat\Image;
use \Exception;



class SubscriptionsTable extends Table {



	public function initialize(array $config) : void {

		$this->setEntityClass(Subscription::class);

		$this->addBehavior('Timestamp', [
			'events' => [
				'Model.beforeSave' => [
					'subscription_created' => 'new'
				]
			]
		]);

		$this->hasMany('Episodes')
		   ->setForeignKey('episode_subscription_id')
		   ->setSort(['episode_pubdate' => 'desc'])
	   		// Episodes shall be deleted when the subscription is deleted
		   ->setDependent(true); 

		$this->belongsTo('Users')
			->setForeignKey('subscription_user_id');

		$this->EpisodesTable = (new \Cake\ORM\Locator\TableLocator())->get('Episodes');
		$this->EpisodesTable->setEntityClass(Episode::class);
		// $this->belongsTo('User');
	}



	/**
	 * Add a podcast subscription
	 *
	 * @param string url
	 * @return bool
	 */
	public function add(string $url): bool {
		$entity = $this->newEntity([
			'subscription_url' => $url
		]);
		$subscription = $this->save($entity);
		if ($subscription !==  false) {
			return $this->update($subscription);
		}
		return false;
	}
	




	public function update(Entity $subscription) : bool {
		
		$ch = curl_init($subscription->subscription_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		// TODO: Handle Errors!!!
		$xmlString = curl_exec($ch);
		try {
			$data = Xml::toArray(Xml::build($xmlString));
		}
		catch (Exception $e) {
			Log::write('warning', sprintf('Failed to update Subscription #%u: %s', $subscription->id, $e->getMessage()));
			// throw new Exception($e);
			return false;
		}
		

		if (!isset($data['rss']['channel'])) {
			throw new Exception('Invalid channel data');
		}

		$channel = $data['rss']['channel'];

		$this->patchEntity($subscription, [
			'subscription_title' => $channel['title'],
			'subscription_description' => $channel['description'],
			'subscription_link' => $channel['link'],
			'subscription_generator' => $channel['generator'],
			'subscription_language' => $channel['language'],
		]);

		$imageFileName = $this->fetchImage($channel['itunes:image']['@href']);
		$subscription->subscription_image = $imageFileName;

		foreach ($channel['item'] as $item) {
			// Log::write('debug', 'Episode title length = ' . strlen($item['title']));

			if (!empty($item['guid']) && is_string($item['guid'])) {
				$guid = $item['guid'];
			}
			else if (!empty($item['guid']['@']) && is_string($item['guid']['@'])) {
				$guid = $item['guid']['@'];
			}

			if ($subscription->findEpisodeByGuid($guid) === null) {
				$duration = 0;
				if (preg_match('/(\d+)(\:(\d+))?(\:(\d+))?/', $item['itunes:duration'], $matches)) {
					for ($i = 1; $i < count($matches); $i+=2) {
						$match = intval($matches[$i]);
						$duration = $duration * 60 + $match;
					}
				}

				$episode = $this->EpisodesTable->newEntity([
					'episode_title' => $item['title'],
					'episode_description' => $item['description'],
					'episode_link' => $item['link'],
					'episode_pubdate' => strftime('%F %T', strtotime($item['pubDate'])),
					'episode_guid' => $guid,
					'episode_duration' => $duration,
					'episode_url' => $item['enclosure']['@url']
				]);
				if (empty($subscription->episodes)) {
					$subscription->episodes = [];
				}
				array_push($subscription->episodes, $episode);
			}
		}
		$subscription->setDirty('episodes', true);

		// echo '<pre>'; var_dump($subscription); echo '</pre>'; die();

		$subscription->subscription_updated = strftime('%F %T');
		$retval = ($this->save($subscription, ['associated' => ['Episodes']]));
		return $retval !== false;
	}



	public function import(string $xmlString) {
		$data = Xml::toArray(Xml::build($xmlString));
		if (!isset($data['opml']['body']['outline'])) {
			throw new Exception('Invalid OPML file');
		}
		$feeds = $data['opml']['body']['outline'];
		foreach ($feeds as $n => $feed) {
			$subscription = $this->newEntity([
				'subscription_title' => $feed['@text'],
				'subscription_url' => $feed['@xmlUrl']
			]);
			$errors = $subscription->getErrors();
			if (empty($errors)) {
				if (!$this->save($subscription)) {
					throw new Exception("Failed to save subscription");
				}
			}
		}
	}


	/**
	 * Fetch image from URL
	 *
	 * @param string 	URLL
	 * @return ?string 	filename or null in case of an error
	 * @throws Exception
	 */
	protected function fetchImage(string $url) : ?string {
		$destPath = PATHTOWEBROOT.'media/subscriptions/';
		$urlParts = parse_url($url);
		$destFileName = basename($urlParts['path']);
		$destFile= $destPath . $destFileName;

		$imageData = file_get_contents($url);
		if (!$imageData) {
			throw new Exception("Failed to load image from {$url}");
		}
		if (!file_put_contents($destFile, $imageData)) {
			throw new Exception("Could not write image data to {$destFile}");
		}

		$mimeType = mime_content_type($destFile);
		if (!$mimeType) {
			throw new Exception("Failed to determine image type for {$destFile}");
		}

		if (!preg_match('/^image\/(.*)$/', $mimeType, $matches)) {
			throw new Exception("Invalid mime type: ${mimeType} for image file ${destFile}");
		}
		$destFileName .= '.'.$matches[1];
		rename($destFile, $destPath.$destFileName);

		$Image = new Image();
		$Image->createThumbnail([
			'sourceImage' => $destPath.$destFileName,
			'destinationImage' => $destPath . 'thumbnails/' . $destFileName,
			'width' => 500
		]);
		return $destFileName;
	}
}
