<?php
namespace Podsblitz\Table;

use \Cake\ORM\Table;
use \Cake\Validation\Validator;
use Contentomat\CakeModelTrait;

class AppTable extends Table {
	use CakeModelTrait;
}