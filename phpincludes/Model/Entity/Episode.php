<?php
namespace Podsblitz\Entity;

use Cake\ORM\Entity;
use \DOMDocument;

class Episode extends Entity {



	public function _getEpisodeImage() {

		$doc = new DOMDocument();
		$doc->loadHTML($this->episode_description);
		$images = $doc->getElementsByTagName('img');
		if ($images->length > 0) {
			$image = $images->item(0);
            //
			// echo '<pre>'; var_dump($images); echo '</pre>'; die();

			return $image->getAttribute('src');

			// echo '<pre>'; var_dump($image->getAttribute('src')); echo '</pre>'; die();
            //
		}
		else {
			return null;
		}
	}
}
