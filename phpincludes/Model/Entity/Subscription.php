<?php
namespace Podsblitz\Entity;

use Cake\ORM\Entity;

class Subscription extends Entity {

	public function findEpisodeByGuid(string $guid): ?Entity {
		foreach ((array)$this->episodes as $episode) {
			if ($episode->episode_guid == trim($guid)) {
				return $episode;
			}
		}
		return null;
	}
}
