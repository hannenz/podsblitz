<?php
namespace Podsblitz\Entity;

use Cake\ORM\Entity;

class Playlist extends Entity {

	protected function _getLength() {
		$episodes = explode(',', $this->playlist_episodes);
		return sprintf('[[%u]]', count($episodes));
	}
}
