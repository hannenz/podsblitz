<?php
namespace Podsblitz;

use Podsblitz\AppController;



class EpisodesController extends AppController {

	public function init() {
		parent::init();
		$this->parserType = 'php';
	}


	/**
	 *  A feed of the latest episodes
	 */
	public function index() {
		$episodes = $this->Episodes->find('all')
							 ->contain(['Subscriptions'])
							 ->order(['episode_pubdate' => 'DESC'])
							 ->limit(10);
		// echo '<pre>'; var_dump($episodes->toArray()); echo '</pre>'; die();
		$this->set('episodes', $episodes);
	}


	public function view() {
		$id = (int)array_shift($this->params);
		$episode = $this->Episodes->get($id, [
			'contain' => ['Subscriptions']
		]);
		$this->set('episode', $episode);
		// $this->set($episode->subscription);
	}



	public function getEpisode() {
		$this->setAutoRender(false);
		$this->isJson = true;
		$id = (int)array_shift($this->params);
		$episode = $this->Episodes->get($id);
		$this->content = $episode->toArray();
	}


	public function markCompleted() {
		$id = (int)array_shift($this->params);
		$value = (int)array_shift($this->params);

		$episode = $this->Episodes->get($id);
		$episode->episode_completed = $value;
		$this->Episodes->save($episode);

		$this->CmtPage->redirectToURL('/de/Episodes/index');
	}
}

isset($_REQUEST['ctl']) or $content = (new EpisodesController())->work();
