<?php
/**
 * Content-o-mat: CMS & Web Application Framework (https://www.content-o-mat.de)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Josef Hahn, Johannes Braun, Carsten Coull
 * @link      https://www.content-o-mat.de Content-o-mat Project
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace Podsblitz;

use Contentomat\Controller;
use Contentomat\CmtPage;
use Contentomat\CakifyTrait;
use Podsblitz\Table\PlaylistsTable;

/**
 * This class can be used to extend Contentomat's controller with project specific
 * methods
 *
 * @class AppController
 * @author Carsten Coull <c.coull@agentur-halma.de>, Johannes Braun <j.braun@agentur-halma.de>
 * @package contentomat
 * @version 2022-02-09
 */
class AppController extends Controller {

	/**
	 * @var Contentomat\CmtPage
	 */
	protected $CmtPage;


	use CakifyTrait {
		init as traitInit;
	}


	/**
	 * @var array
	 */
	protected $playlists;


	public function init() {
		parent::init();
		$this->traitInit();

		$this->CmtPage = new CmtPage();

		$this->PlaylistsTable = $this->Locator->get('Playlists');
		$this->playlists = $this->PlaylistsTable->find('all');
		$this->set('playlists', $this->playlists->toArray());
	}


	public function initActions($action = '') {
		parent::initActions($action);

	}
}