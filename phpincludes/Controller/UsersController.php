<?php
namespace Podsblitz;

use Podsblitz\AppController;


class UsersController extends AppController {

	public function init() {
		parent::init();
		$this->parserType = 'php';
	}


	/**
	 *  A feed of the latest episodes
	 */
	public function index() {
		$users = $this->Users->find('all')
							 ->contain(['Subscriptions', 'Playlists']);
		$this->set('users', $users);
	}
}

isset($_REQUEST['ctl']) or $content = (new UsersController())->work();
