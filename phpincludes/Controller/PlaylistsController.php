<?php
namespace Podsblitz;

use Podsblitz\AppController;
use Contentomat\CmtPage;
use Cake\Log\Log;

use Exception;



class PlaylistsController extends AppController {


	protected $CmtPage;


	public function init() {
		parent::init();


		$this->CmtPage = new CmtPage();
		$this->parserType = 'php';
	}


	/**
	 *  A feed of the latest episodes
	 */
	public function index() {
		$playlists = $this->Playlists->find('all', ['contain' => 'Episodes']);
		$this->set('playlists', $playlists);
	}


	public function view() {
		$id = (int)array_shift($this->params);
		$playlist = $this->Playlists->get($id, ['contain' => ['Episodes' => 'Subscriptions']]);
		$this->set('playlist', $playlist);
		$this->set('episodes', $playlist->episodes);
	}


	public function add() {
		try {
			if (!empty($this->postvars)) {
				$playlist = $this->Playlists->newEmptyEntity();
				$playlist->playlist_title = $this->postvars['playlist_title'];
				$playlist->playlist_description = $this->postvars['playlist_description'];
				$this->Playlists->save($playlist);
				$this->CmtPage->redirectToURL(sprintf('/%s/Playlists/index', $this->pageLang));
			}
		}
		catch (Exception $e) {
			// ... Flash Message?
			die ($e->getMessage());
		}
		$this->set($this->postvars);
	}


	public function delete() {
		try {
			if (strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
				throw new Exception('Illegal method');
			}
			$id = (int)array_shift($this->params);
			$entity = $this->Playlists->get($id);
			$this->Playlists->delete($entity);
		}
		catch (Exception $e) {
			// ... Flash Message?
			die($e->getMessage());
		}
		$this->CmtPage->redirectToURL(sprintf('/%s/Playlists/index', $this->pageLang));
	}



	public function addEpisode() {
		$playlistId = array_shift($this->params);
		$episodeId = array_shift($this->params);

		try {
			$episode = $this->Playlists->Episodes->get($episodeId);
			$playlist = $this->Playlists->get($playlistId, ['contain' => 'Episodes']);
			$playlist->episodes = [$episode];
			$playlist->setDirty('episodes', true);
			$this->Playlists->save($playlist);
		}
		catch (Exception $e) {
			die($e->getMessage());
		}
		$this->set(compact('playlistId', 'episodeId'));
	}



	public function removeEpisode() {
		$playlistId = array_shift($this->params);
		$episodeId = array_shift($this->params);

		try {
			$playlist = $this->Playlists->get($playlistId);
			$episodes = explode(',', $playlist->playlist_episodes);
			$n = array_search($episodeId, $episodes);
			if ($n) {
				unset($episodes[$n]);
			}
			$playlist->playlist_episodes = join(',', $episodes);
			$this->Playlists->save($playlist);
		}
		catch (Exception $e) {
		}
		$this->CmtPage->redirectToURL('/de/Playlists/index');
	}
}
isset($_REQUEST['ctl']) or $content = (new PlaylistsController())->work();
