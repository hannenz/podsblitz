<?php
namespace Podsblitz;

use Podsblitz\AppController;
use Exception;
use Contentomat\Logger;
use Contentomat\FileHandler;
use Cake\Utility\Xml;
use Cake\Log\Log;


class SubscriptionsController extends AppController {


	public function init() {
		parent::init();
		$this->setPageTitle(' Podsblit🗲 | Abos');
	}


	public function index() {
		$subscriptions = $this->Subscriptions->find('all');
		$this->set('subscriptions', $subscriptions);
	}


	public function view() {
		$this->parserType = 'php';
		$id = array_shift($this->params);
		$subscription = $this->Subscriptions->get($id, [
			'contain' => [ 'Episodes' ]
		]);
		$this->setPageTitle('Podsblit🗲 | ' . $subscription->subscription_title);
		$this->set('subscription', $subscription);
		$this->set('episodes', $subscription->episodes);
	}



	/**
	 * undocumented function
	 *
	 * @return void
	 */
	public function add() {
		if (!empty($this->postvars)) {
			if (!$this->Subscriptions->add($this->postvars['url'])) {
				die ("Failed to add subscription");
			}
		}
		$this->changeAction('index');
	}
	


	/**
	 * undocumented function
	 *
	 * @return void
	 */
	public function update() {
		$id = (int)array_shift($this->params);

		$subscription = $this->Subscriptions->get($id, [
			'contain' => ['Episodes']
		]);

		$success = $this->Subscriptions->update($subscription);
		$this->set($subscription);
		$this->set('success', $success);

		// $this->setAutoRender(false);
		// return $this->CmtPage->redirectToURL("/de/Subscriptions/view/${id}");
	}
	

	public function updateAll() {
		$subscriptions = $this->Subscriptions->find('all', [
			'contain' => ['Episodes']
		]);
		foreach ($subscriptions as $subscription) {
			Log::write('debug', "Updating #{$subscription->id} [{$subscription->subscription_title}]");
			$this->Subscriptions->update($subscription);
		}
		$this->CmtPage->redirectToURL('/de/Subscriptions/index');
	}


	public function delete() {
		$id = (int)array_shift($this->params);
		$subscription = $this->Subscriptions->get($id);
		$this->Subscriptions->delete($subscription);
		// Delete all episodes
		// Delete episodes from playlists
		$this->CmtPage->redirectToURL('/de/Subscriptions/index');
	}


	public function import() {
		if (!empty($_POST)) {
			try {
				if (!is_uploaded_file($_FILES['file']['tmp_name'])) {
					throw new \Exception("File is not readable");
				}

				$xml = file_get_contents($_FILES['file']['tmp_name']);
				$this->Subscriptions->import($xml);
			}
			catch (Exception $e) {
				echo ($e->getMessage()) . '<br>';
			}
			$this->changeAction('index');
		}
	}


	public function export() {
		try {
			$subscriptions = $this->Subscriptions->find('all');
			$data = [ 'body' => ['outline' => []] ];
			foreach ($subscriptions as $subscription) {
				$data['body']['outline'][] = [
					'@type' => 'rss',
					'@text' => '',
					'@title' => $subscription['subscription_title'],
					'@xmlUrl' => $subscription['subscription_url'],
					'@htmlUrl' => $subscription['subscription_link']
				];
			}
			$options = [
				'pretty' => true,
				'encoding' => 'utf8'
			];
			$xmlObject = Xml::fromArray($data, $options);
			$xmlString = $xmlObject->asXML();
			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false); // required for certain browsers
			header("Content-Type: text/xml");
			header("Content-Disposition: attachment; filename=podsblitz.".strftime('%F-%H%M').".opml");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".strlen($xmlString));
			die($xmlString);
		}
		catch (\Exception $e) {
			die($e->getMessage());
		}
	}
}

// If called as pageless action, we don't need to call `work` method manually,
// it will be called from `index.php` und this executed twice.
isset($_REQUEST['ctl']) or $content = (new SubscriptionsController())->work();
