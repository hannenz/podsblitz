/**
 * src/js/modules/player.js
 *
 * @author Johannes Braun <hannenz@posteo.de>>
 * @package podsblitz
 * @version 2022-10-07
 */
export default class Player {

	constructor() {
		this.player = document.getElementById('player');
		this.audio = this.player.querySelector('audio');
		this.currentTitle = this.player.querySelector('.player__current-title');
		this.currentTime = this.player.querySelector('.player__current-time');
		this.playBtn = this.player.querySelector('.player__btn--play');
		this.playBtn.addEventListener('click', this.onPlayBtnClicked.bind(this));
		this.seekBackBtn = this.player.querySelector('.player__btn--seek-back');
		this.seekFwdBtn = this.player.querySelector('.player__btn--seek-fwd');
		this.seekBackBtn.addEventListener('click', e => {
			this.audio.currentTime -= 15;
		});
		this.seekFwdBtn.addEventListener('click', e => {
			this.audio.currentTime += 15;
		});

		this.audio.addEventListener('play', this.onAudioPlaying.bind(this));
		this.audio.addEventListener('pause', this.onAudioPaused.bind(this));
		this.audio.addEventListener('timeupdate', e => {
			const time = parseInt(this.audio.currentTime);
			const minutes = parseInt(time / 60);
			const seconds = time %60;
			const m_str = ((minutes < 10) ? '0' : '') + minutes;
			const s_str = ((seconds < 10) ? '0' : '') + seconds;
			
			this.currentTime.innerText = `${m_str}:${s_str}`;
		});

		this.rateBtns = document.querySelectorAll('[name="rate"]');
		this.rateBtns.forEach((btn) => {
			btn.onclick = (ev) => this.audio.playbackRate = parseFloat(ev.currentTarget.value); 
		});
	}


	play() {
		this.audio.play();
	}



	async setEpisode(episodeId) {

		return new Promise((resolve) => {
			fetch('/de/Episodes/getEpisode/' + episodeId)
			.then(response => response.json())
			.then(data => {
				this.audio.src = data.episode_url;
				this.currentTitle.innerText = data.episode_title;
				
				this.audio.dataset.episodeId = episodeId;
				resolve();
			})
			.catch((err) => {
				console.error(`Failed to fetch episode #${episodeId}`);
			});
		});
	}

	// Save state
	persist() {
		localStorage.setItem('player-src', this.audio.src);
		localStorage.setItem('player-is-playing', !this.audio.paused);
		localStorage.setItem('player-current-time', this.audio.currentTime);
		localStorage.setItem('player-episode-id', this.audio.dataset.episodeId);
		localStorage.setItem('player-playlist', this.audio.dataset.playlist);
	}


	restore() {
		this.audio.src = localStorage.getItem('player-src');
		const isPlaying = localStorage.getItem('player-is-playing') === 'true';
		const episodeId = localStorage.getItem('player-episode-id');
		this.audio.dataset.episodeId = episodeId;
		this.audio.dataset.playlist = localStorage.getItem('player-playlist');
		this.setEpisode(episodeId).then(() => {
			if (isPlaying) {
				this.audio.currentTime = localStorage.getItem('player-current-time');
				this.audio.play();
			}
		});
	}


	onPlayBtnClicked(e) {

		if (this.audio.paused) {
			this.audio.play();
		}
		else {
			this.audio.pause();
		}
	}

	onAudioPlaying() {
		this.playBtn.innerHTML = '<svg class="icon"><use href="/dist/img/icons.svg#pause"></use></svg>';
	}
	onAudioPaused() {
		this.playBtn.innerHTML = '<svg class="icon"><use href="/dist/img/icons.svg#play"></use></svg>';
	}
}
