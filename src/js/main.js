/**
 * src/js/main.js
 *
 * @author Johannes Braun <hannenz@posteo.de>>
 * @package podsblitz
 * @version 2022-10-07
 */
import Player from '/dist/js/modules/player.js';

/**
 * Application main class
 */
class App {

	/**
	 * Constructor
	 */
	constructor() {

		// Class properties, can only be declared inside a method … :-(
		this.debug = false;

		document.addEventListener('DOMContentLoaded', () => this.init());
		window.onload = () => this.pageInit();
	}



	/**
	 * init
	 * Called on DOMContentLoaded, so a good place to setup things not dependend
	 * on the page finished rendering
	 */
	init() {
		if (this.debug) {
			console.debug('APP::init');
		}

		document.body.classList.add('page-has-loaded');

		window.addEventListener('resize', () => this.throttle(this.resizeHandler));
		window.addEventListener('scroll', () => this.throttle(this.scrollHandler));
	}



	/**
	 * pageInit
	 * Called on window.load, i.e. when the page and all assets have been
	 * loaded completely and the page has rendered
	 *
	 * @return void
	 */
	pageInit() {

		if (this.debug) {
			console.debug('APP::pageInit');
		}

		document.body.classList.add('page-has-rendered');

		// Let's see if we have a header element and get it's height (for
		// detecting scroll past header, see `App.scrollHandler`
		this.header = document.querySelector('header');
		if (this.header) {
			let rect = this.header.getBoundingClientRect();
			this.headerBottom = rect.top + rect.height;
		}

		this.main();
	}


	/**
	 * Main method
	 * Put main business logic here
	 *
	 * @return void
	 */
	main() {

		this.player = new Player();
		this.player.restore();

		window.onbeforeunload = (ev) => {
			console.log('persisting');
			this.player.persist();
		}

		const episodePlayBtns = document.querySelectorAll('.episode-play-btn');
		for (var i = 0; i < episodePlayBtns.length; i++) {
			episodePlayBtns[i].addEventListener('click', this.onEpisodePlayBtnClicked.bind(this));
		}
	}


	onEpisodePlayBtnClicked(ev) {
		this.player.setEpisode(ev.currentTarget.dataset.episodeId).then(() => {
			this.player.currentTime = 0;
			this.player.play();
		});
	}


	/*
	 * Debounced / throttled scroll handler. Put your scroll-related stuff here!
	 * @return void
	 */
	scrollHandler() {
		let y = window.scrollY;

		if (this.debug) {
			console.debug(`Scroll position: ${y}`);
		console.log(ev.currentTarget);
		console.log(ev.currentTarget);
		}

		// Set classes on body depending on how far the page has scrolled
		document.body.classList.toggle('has-scrolled', y > 0);
		document.body.classList.toggle('has-scrolled-a-bit', y > 30);
		document.body.classList.toggle('has-scrolled-past-header', y > this.headerBottom);
		document.body.classList.toggle('has-scrolled-100vh', y > window.innerHeight);

		// Todo: Scroll direction!

		// Extend here …
	}


	/**
	 * Debounced / throttled scroll handler. Put your resize-related stuff here!
	 *
	 * @return void
	 */
	resizeHandler() {
		let width = window.innerWidth,
			height = window.innerHeight;

		if (this.debug) {
			console.debug(`Window has been resized to ${width}, ${height}`);
		}

		// Set custom properties
		document.body.style.setProperty('--window-width', `${width}px`);
		document.body.style.setProperty('--window-height', `${height}px`);

		// Extend here …
	}


	/**
	 * Throttler method
	 *
	 * @param callable: Handler to be called on throttled scroll event
	 * @return void
	 */
	throttle(handler) {

		this.ticking = false;

		if (!this.ticking) {
			window.requestAnimationFrame(() => {
				handler.call(this);
				this.ticking = false;
			});
			this.ticking = true;
		}
	}


	/**
	 * execute if click/interaktion is outside of the Selector
	 *
	 * @param selector: Element selector (e.g. class, id) to watch for
	 * @param callback: function to execute if interaktion is outside of the Selector
	 *
	 * Example:
	 * this.app.onOutsideClick('#main-menu', this.closeMenu);
	 */
	onOutsideClick(selector, callback) {
		document.addEventListener('click', (event) => {
			if (event.target.parentNode.closest(selector) == null) {
				callback();
			}
		});
	};
};

new App();
