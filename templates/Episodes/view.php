<div id="backdrop" style="filter: blur(25px); position: fixed; top: 0; left: 0; width: 100vw; height: 100vh; z-index: -1; background-size: 100%; background-image:url(/media/subscriptions/<?php echo $episode->subscription->subscription_image?>)"></div>
<section class="stack">
	<header class="stack">
		<h1><?php echo $episode->episode_title; ?></h1>
		<p><?php echo $episode->episode_description; ?></p>
	</header>
	<div class="stack episode-panel">
		<div><small>{DATEFMT:"{VAR:episode_pubdate}":"%d.%m.%Y %T"}</small></div>
		<p>
			<button 
				id="play-button"
				class="episode-play-btn" 
				data-episode-id="<?php echo $episode->id; ?>"
				>
				<svg class="icon"><use href="/dist/img/icons.svg#play"></use></svg>
				<span>Abspielen</span>
			</button>
		</p>
		<ul>
			<li>
				<a href="/de/Subscriptions/view/<?php echo $episode->episode_subscription_id; ?>">
				<svg class="icon" viewBox="0 0 100 100"><use href="/dist/img/icons.svg#arrow-left"></use></svg>
				Zurück zu &bdquo;<?php echo $episode->subscription->subscription_title; ?>&rdquo;</a>
			</li>
			<li>
				<a href="/de/Episodes/markCompleted/<?php echo $episode->id; ?>/<?php echo 1 - $episode->episode_completed; ?>">
				<svg class="icon" viewBox="0 0 100 100"><use href="/dist/img/icons.svg#check-circle"></use></svg>
				Als <?php if ($episode->episode_completed): ?>nicht <?php endif; ?>abgeschlossen markieren
				</a>
			</li>
		</ul>
	</div>
</section>
