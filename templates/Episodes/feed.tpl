<ul style="list-style: none">
	{LOOP VAR(episodes)}
	<li>
		<div class="episode stack" style="border: 1px solid gainsboro; padding: 0.5rem 1rem 1rem 1rem">
			<figure><img width="100" src="/media/subscriptions/thumbnails/{VAR:subscription.subscription_image}" /></figure>
			<p><small>{DATEFMT:"{VAR:episode_pubdate}":"%d.%m.%Y %T"}</small></p>
			<h2>{VAR:episode_title}</h2>
			<div>{VAR:episode_description:strip_tags}</div>
			<p><a href="/de/Episodes/view/{VAR:id}">Details</a></p>
			<p>Subscription: {VAR:subscription.subscription_title}</p>
		</div>
	</li>
	{ENDLOOP VAR}
</ul>
