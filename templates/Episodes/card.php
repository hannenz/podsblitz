<div class="card episode-teaser <?php if ($episode->episode_completed): ?>episode-teaser--completed<?php endif; ?>">
	<!-- <img width="100" src="/media/subscriptions/<?php echo $episode->subscription->subscription_image;?>" alt="" /> -->
	<div class="card__image">
		<img width="100" height="100" src="<?php echo $episode->episode_image ?? '/media/subscriptions/'.$episode->subscription->subscription_image; ?>" alt="" />
		<div class="card__meta">
			<?php echo $episode->episode_pubdate->nice(); ?>
		</div>
	</div>
	<div class="card_body stack">
		<h3 class="card__title"><?php echo $episode->episode_title; ?></h3>
		<div class="card__subtitle"><?php echo $episode->subscription->subscription_title; ?></div>
		<div class="card__description">
			<?php echo Cake\Utility\Text::truncate($episode->episode_description, 100, ['exact' => false, 'html' => true, 'ellipsis' => ' &hellip;']); ?>
		</div>
	</div>
	<div class="card__action-area">
		<a href="/{PAGELANG}/Episodes/view/<?php echo $episode->id; ?>">
			<svg class="icon"><use href="/dist/img/icons.svg#arrow-right"></use></svg>
			Details
		</a>
		<button class="episode-play-btn" data-episode-id="<?php echo $episode->id; ?>">
			<svg class="icon"><use href="/dist/img/icons.svg#play"></use></svg>
			Abspielen
		</button>
		<details class="dropdown">
			<summary>
				<svg class="icon"><use href="/dist/img/icons.svg#plus"></use></svg>
				Zu Playlist hinzufügen
			</summary>
			<ul class="menu menu--vertical">
				<?php foreach ($playlists as $playlist): ?>
					<li class="menu__item">
						<a href="/de/Playlists/addEpisode/<?php echo $playlist->id; ?>/<?php echo $episode->id; ?>" class="menu__link">
							<?php echo $playlist->playlist_title; ?>
							<!--span class="badge"><?php echo $playlist->length; ?></span-->
						</a>
					</li>
				<?php endforeach ?>
			</ul>
		</details>
	</div>
</div>
