<ul>
	<?php foreach ($users as $user): ?>
		<li>
			<dl>
				<dt>Username</dt>
				<dd><?php echo $user->cmt_username; ?></dd>
				<dt>Alias</dt>
				<dd><?php echo $user->cmt_useralias; ?></dd>
				<dt>Abos</dt>
				<dd><?php echo count($user->subscriptions); ?></dd>
				<dt>Playlists</dt>
				<dd><?php echo count($user->playlists); ?></dd>
			</dl>
		</li>
	<?php endforeach ?>	
</ul>
