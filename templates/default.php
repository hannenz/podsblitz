<!doctype html>
<html class="no-js" lang="{PAGELANG}">
<head>
	<link rel="stylesheet" type="text/css" href="/dist/css/main.css">
	{INCLUDE:PATHTOWEBROOT."templates/partials/head.tpl"}
</head>
<body>
	<a href="#main-content" class="skip-link visually-hidden">{I18N:common.skip-navigation}</a>

	<?php include(PATHTOWEBROOT.'templates/partials/header.php'); ?>

	<main id="main-content" class="main-content">
		{LOOP CONTENT(1)}{ENDLOOP CONTENT}
	</main>

	<?php include(PATHTOWEBROOT.'templates/partials/footer.php'); ?>

	{IF(!{LAYOUTMODE})}
		<script type="module" src="/dist/js/main.js"></script>
	{ENDIF}
	{LAYOUTMODE_ENDSCRIPT}
</body>
</html>
