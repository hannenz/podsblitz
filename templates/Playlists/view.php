<div class="playlist">
	<h3><?php echo $playlist->playlist_title; ?></h3>
	<div><?php echo $playlist->playlist_description; ?></div>
	<div><?php echo $playlist->playlist_created; ?></div>
	<div>
		<h4><?php echo count($episodes); ?> Episoden</h4>
		<ol>
			<?php
			foreach ($episodes as $episode) {
				include(PATHTOWEBROOT.'templates/Episodes/card.php');
			}
			?>
		</ol>
	</div>
	<div>
		<a href="/{PAGELANG}/Playlists/edit/<?=$playlist->id?>"><svg class="icon"><use href="/dist/img/icons.svg#edit"></use></svg> bearbeiten</a>
		<form action="/{PAGELANG}/Playlists/delete/<?=$playlist->id;?>" method="post"><button class="button" type="submit"><svg class="icon"><use href="/dist/img/icons.svg#trash"></use></svg> löschen</button></form>
	</div>
</div>

