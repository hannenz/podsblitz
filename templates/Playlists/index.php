<div>
<a href="/{PAGELANG}/Playlists/add" class="button pill"> <svg class="icon"><use href="/dist/img/icons.svg#plus"></use></svg> Neue Playlist erstellen</a>
</div>
<ul class="playlists">
	<?php foreach ($playlists as $playlist): ?>
		<li>
			<div class="playlist">
				<h3><?php echo $playlist->playlist_title; ?></h3>
				<div><?php echo $playlist->playlist_description; ?></div>
				<div><?php echo $playlist->playlist_created; ?></div>
				<div>
					<h4>Episoden</h4>
					<ol>
						<?php
						foreach ($playlist->episodes as $episode) {
							include(PATHTOWEBROOT.'templates/episodes/card.php');
						}
						?>
					</ol>
				</div>
				<div>
					<a href="/{PAGELANG}/Playlists/view/<?php echo $playlist->id; ?>">Details</a>
					<a href="/{PAGELANG}/Playlists/edit/<?php echo $playlist->id; ?>"><svg class="icon"><use href="/dist/img/icons.svg#edit"></use></svg> bearbeiten</a>
					<form action="/{PAGELANG}/Playlists/delete/<?php echo $playlist->id; ?>" method="post"><button class="button" type="submit"><svg class="icon"><use href="/dist/img/icons.svg#trash"></use></svg> löschen</button></form>
				</div>
			</div>
		</li>
	<?php endforeach ?>
</ul>
