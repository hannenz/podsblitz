<form action="/{PAGELANG}/Playlists/add" method="post" class="stack">

	<h2>Neue Playlist</h2>
	<div class="form-field">
		<label for="playlist-title">Titel</label><br>
		<input id="playlist-title" name="playlist_title" value="<?php $playlist_title; ?>" required>
	</div>
	<div class="form-field">
		<label for="playlist-description">Beschreibung</label><br>
		<textarea id="playlist-description" name="playlist_description"><?php $playlist_description; ?></textarea>
	</div>
	<div class="form-field form-field--submit">
		<button class="button" type="submit">Erstellen</button>
	</div>
</form>
