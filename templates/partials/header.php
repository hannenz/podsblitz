<header class="main-header">
	<a href="/{PAGELANG}/Episodes/index" class="brand">
		<svg class="icon"><use href="/dist/img/icons.svg#zap"></use></svg>
		Podsblitz
	</a>
	<nav class="nav">
		<ul class="menu">
			<li class="menu__item <?php if (preg_match('/Episodes\/index/', $_SERVER['REQUEST_URI'])):?>menu__item--current<?php endif ?>"><a class="menu__link" href="/{PAGELANG}/Episodes/index">Feed</a></li>
			<li class="menu__item <?php if (preg_match('/Subscriptions/', $_SERVER['REQUEST_URI'])):?>menu__item--current<?php endif ?>"><a class="menu__link" href="/{PAGELANG}/Subscriptions/index">Meine Podcasts</a></li>
			<li class="menu__item <?php if (preg_match('/Playlists/', $_SERVER['REQUEST_URI'])):?>menu__item--current<?php endif ?>"><a class="menu__link" href="/{PAGELANG}/Playlists/index">Playlists</a></li>
			<li class="menu__item"><a class="menu__link button button--primary button--with-icon pill" href="/{PAGELANG}/Subscriptions/add"> <svg class="icon"><use href="/dist/img/icons.svg#plus"></use></svg> Podcast hinzufügen</a></li>
		</ul>
	</nav>
</header>
