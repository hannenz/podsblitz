<div id="player" class="player">
	<audio id="player-audio" data-episode-id="0" data-playlist=""></audio>
	<div class="player__controls player__controls--play">
		<div class="btn-group">
			<button class="player__btn player__btn--seek-back">-15</button>
			<button class="player__btn player__btn--play">
				<svg class="icon"><use href="/dist/img/icons.svg#play"></use></svg>
			</button>
			<button class="player__btn player__btn--seek-fwd">+15</button>
		</div>
	</div>
	<div class="player__controls player__controls--rate">
		<div class="btn-group">
			<div><input type="radio" value="0.5" name="rate" id="rate-1"><label for="rate-1">0.5 &times;</label></div>
			<div><input type="radio" value="1" name="rate" id="rate-2"><label for="rate-2">1 &times;</label></div>
			<div><input type="radio" value="1.5" name="rate" id="rate-3"><label for="rate-3">1.5 &times;</label></div>
			<div><input type="radio" value="2" name="rate" id="rate-4"><label for="rate-4">2 &times;</label></div>
		</div>
	</div>
	<div class="player__current-title"></div>
	<div class="player__current-time"></div>
</div>
