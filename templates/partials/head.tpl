	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{IF({ISSET:PAGEMETATITLE})})}{PAGEMETATITLE}{ELSE}{PAGETITLE} - {CONSTANT:WEBNAME}{ENDIF}</title>

	<script type="module">
		document.documentElement.classList.remove('no-js');
		document.documentElement.classList.add('js');
	</script>

	<!-- <link rel="stylesheet" href="/dist/css/main.css" /> -->
	<!-- <link rel="stylesheet" href="/dist/css/print.css" media="print" /> -->

	<meta name="description" content='{IF("{PAGEMETADESCRIPTION}" != "")}{PAGEMETADESCRIPTION}{ELSE}{PAGEVAR:cmt_meta_description:recursive}{ENDIF}'>
	<meta name="keywords" content='{IF("{PAGEMETADESCRIPTION}" != "")}{PAGEMETADESCRIPTION}{ELSE}{PAGEVAR:cmt_meta_keywords:recursive}{ENDIF}'>

	<meta name="author" content="">

	<meta property="og:title" content='{IF({ISSET:PAGEMETATITLE})})}{PAGEMETATITLE}{ELSE}{PAGETITLE} - {CONSTANT:WEBNAME}{ENDIF}'>
	<meta property="og:description" content='{IF("{PAGEMETADESCRIPTION}" != "")}{PAGEMETADESCRIPTION}{ELSE}{PAGEVAR:cmt_meta_description:recursive}{ENDIF}'>
	<meta property="og:image" content='{IF("{PAGEMETAIMAGE}" != "")}{PAGEMETAIMAGE}{ELSE}https://{ENVVAR:SERVER_NAME}/{PAGEVAR:cmt_meta_image:recursive}{ENDIF}'>
	<meta property="og:image:alt" content='{IF("{PAGEMETAIMAGEALT}" != "")}{PAGEMETAIMAGEALT}{ELSE}{PAGEVAR:cmt_meta_image_description:recursive}{ENDIF}'>
	<meta property="og:locale" content="{PAGELANG}">
	<meta property="og:type" content="website">
	<meta name="twitter:card" content="summary_large_image">
	<meta property="og:url" content="{CANONICALURL}">

	<link rel="canonical" href="{CANONICALURL}">
	<!-- <link rel="shortcut icon" href="/dist/img/favicons/favicon.ico" /> -->
	<link rel="shortcut icon" href="/dist/img/icons/zap.svg" />

	<meta name="theme-color" content="#ffffff">

	<!-- Schema.org JSON+LD -->
	<script type="application/ld+json">
		{
			"@context": "http://schema.org/",
			"@type": "WebSite",
			"name": "{WEBNAME}",
			"url": "https://{ENVVAR:SERVER_NAME}"
		}
	</script>
	<script type="application/ld+json">
		{
			"@context": "http://schema.org/",
			"@type": "",
			"name": "",
			"legalName": "",
			"url": "https://{ENVVAR:SERVER_NAME}",
			"email": "",
			"telephone": "",
			"address": {
				"@type": "PostalAddress",
				"addressLocality": "",
				"postalCode": "",
				"streetAddress": ""
			}
		}
	</script>

	{LAYOUTMODE_STARTSCRIPT}
