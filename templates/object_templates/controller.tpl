{IF(!{LAYOUTMODE})}
	{EVAL}
	$ctlName = sprintf('\\%s\\%s', 
		APP_NAMESPACE, 
		trim(preg_replace('/\.php$/', '', $cmtContentData['file1']), '/')
	);
	$content = (new $ctlName())->work();
	{ENDEVAL}
{ENDIF}
{IF({LAYOUTMODE})}
	{SCRIPT:1:phpincludes/Controller}
	{LAYOUT OPTION}
		<p class="cmt-label">Action</p>
		<div> {PARAM:1} </div>
	{ENDLAYOUT OPTION}
	{LAYOUT OPTION}
		<p class="cmt-label">Parameter</p>
		<textarea></textarea>
		<span style="display:none">{TEXT:1}</span>
	{ENDLAYOUT OPTION}
{ENDIF}
