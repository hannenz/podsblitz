<figure>
	<img src="/media/subscriptions/thumbnails/<?php echo $subscription->subscription_image; ?>" width="300" />
</figure>
<h1><?php echo $subscription->subscription_title; ?></h1>
<div>
	<?php echo $subscription->subscription_description; ?>
</div>
<p><?php echo count($episodes); ?> Episoden</p>


<ul class="stack">
	<li><a href="/de/Subscriptions/update/<?php echo $subscription->id; ?>"><svg class="icon"><use href="/dist/img/icons.svg#refresh-ccw"></use></svg> Update</a></li>
	<li><form action="/de/Subscriptions/delete/<?php echo $subscription->id; ?>" method="post"><button type="submit"><svg class="icon"><use href="/dist/img/icons.svg#trash"></use></svg> Löschen</button></form></li>
	<li><a href="/de/Subscriptions/index"><svg class="icon"><use href="/dist/img/icons.svg#arrow-left"></use></svg> Zurück</a></li>
</ul>

<ul class="stack" style="list-style: none">
	<?php foreach ($episodes as $episode): ?>
		<li>
			<?php include(PATHTOWEBROOT.'templates/Episodes/card.php'); ?>
			<!-- <div class="episode stack" style="border: 1px solid gainsboro; padding: 0.5rem 1rem 1rem 1rem"> -->
			<!-- 	<p><small>{DATEFMT:"{VAR:episode_pubdate}":"%d.%m.%Y %T"}</small></p> -->
			<!-- 	<h2>{VAR:episode_title}</h2> -->
			<!-- 	<div>{VAR:episode_description}</div> -->
			<!-- 	<p><a href="/de/Episodes/view/{VAR:id}">Details</a></p> -->
			<!-- </div> -->
		</li>
	<?php endforeach ?>
</ul>
