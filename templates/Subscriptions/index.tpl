<div>
	<p>{COUNT:subscriptions} Podcastabonnements</p>
</div>
<div>
	<a href="/de/Subscriptions/updateAll">🗘  Alle aktualisieren</a>
	<a href="/de/Subscriptions/import">&darr; importieren (OPML)</a>
	<a href="/de/Subscriptions/export">&uarr; Exportieren (OPML)</a>
</div>

<ul class="subscriptions" style="list-style: none; display: grid; grid-template-columns: 1fr 1fr 1fr">
	{LOOP VAR(subscriptions)}
		<li>
			<div class="subscription">
				<figure class="subscription__image">
					<a href="/de/Subscriptions/view/{VAR:id}"><img src="/media/subscriptions/thumbnails/{VAR:subscription_image}" width="150" /></a>
					<figcaption>
						<a href="/de/Subscriptions/view/{VAR:id}">{VAR:subscription_title}</a>
					</figcaption>
				</figure>
				<form action="/de/Subscriptions/delete/{VAR:id}"><button type="submit">Löschen</button></form>
			</div>
		</li>
	{ENDLOOP VAR}
</ul>

